--Q1:
CREATE TABLE EMPLOYEE (
	EmpNo INT IDENTITY(1,1) PRIMARY KEY,
	Empname NVARCHAR(100) NOT NULL,
	BirthDay DATETIME NOT NULL,
	DeptNo INT NOT NULL,
	MgrNo INT NOT NULL,
	Salary MONEY NOT NULL,
	Level INT CHECK(Level >= 1 AND Level <= 7) NOT NULL,
	Status INT CHECK ( Status >= 0 AND Status <= 2),
	Note TEXT
	);

CREATE TABLE SKILL (
	SkillNo INT IDENTITY(1,1) PRIMARY KEY,
	SkillName VARCHAR(100) NOT NULL,
	Note TEXT
	);

CREATE TABLE DEPARTMENT (
	DeptNo INT IDENTITY(1,1) PRIMARY KEY,
	DeptName VARCHAR(50) NOT NULL,
	Note TEXT
	);
CREATE TABLE EMP_SKILL(
	SkillNo INT NOT NULL FOREIGN KEY REFERENCES SKILL(SkillNo),
	EmpNo INT NOT NULL FOREIGN KEY REFERENCES EMPLOYEE(EmpNo),
	SkillLevel INT NOT NULL CHECK(SkillLevel >=1 AND SkillLevel <=3),
	RegDate DATETIME NOT NULL,
	Description TEXT,
	CONSTRAINT EMP_SKILL_NO PRIMARY KEY (SkillNo,EmpNo)
	);
--Q2:
ALTER TABLE EMPLOYEE
	ADD Email VARCHAR(50) NOT NULL UNIQUE;

ALTER TABLE EMPLOYEE 
	ADD CONSTRAINT df_MgrNo DEFAULT '0' FOR MgrNo;
ALTER TABLE EMPLOYEE 
	ADD CONSTRAINT df_Status DEFAULT 0 FOR Status;

--Q3:
ALTER TABLE EMPLOYEE ADD CONSTRAINT fk_DeptNo 
	FOREIGN KEY (DeptNo) REFERENCES DEPARTMENT(DeptNo);

ALTER TABLE EMP_SKILL
	DROP COLUMN Description;

--Q4:
INSERT INTO DEPARTMENT (DeptName, Note) VALUES('Nhan Su', '30 TV');
INSERT INTO DEPARTMENT (DeptName, Note) VALUES('Du An', '50 TV');
INSERT INTO DEPARTMENT (DeptName, Note) VALUES('Ke Toan', '5 TV');
INSERT INTO DEPARTMENT (DeptName, Note) VALUES('Chuc Nang', '30 TV');
INSERT INTO DEPARTMENT (DeptName, Note) VALUES('Training', '60 TV');

INSERT INTO EMPLOYEE (Empname,BirthDay,DeptNo,MgrNo,Salary,Level,Status,Note,Email) VALUES('Nguyen Ngoc Anh','4/4/1999','1','22','30000000','1','2','Nhan Vien Cham Chi','ngocanhlbgn@gmail.com');
INSERT INTO EMPLOYEE (Empname,BirthDay,DeptNo,MgrNo,Salary,Level,Status,Note,Email) VALUES('Nguyen Ngoc An','4/5/1999','1','22','20000000','1','2','Nhan Vien Cham Chi','ngocanlbgn@gmail.com');
INSERT INTO EMPLOYEE (Empname,BirthDay,DeptNo,MgrNo,Salary,Level,Status,Note,Email) VALUES('Nguyen Ngoc Phuong','4/6/1999','1','22','10000000','1','2','Nhan Vien Cham Chi','ngocphuonglbgn@gmail.com');
INSERT INTO EMPLOYEE (Empname,BirthDay,DeptNo,MgrNo,Salary,Level,Status,Note,Email) VALUES('Nguyen Ngoc Hanh','4/7/1999','1','22','5000000','1','2','Nhan Vien Cham Chi','ngochanhlbgn@gmail.com');
INSERT INTO EMPLOYEE (Empname,BirthDay,DeptNo,MgrNo,Salary,Level,Status,Note,Email) VALUES('Nguyen Ngoc Nam','4/8/1999','1','22','5000000','1','2','Nhan Vien Cham Chi','ngocnamlbgn@gmail.com');

INSERT INTO SKILL (SkillName,Note) VALUES('Dev', 'cham chi');
INSERT INTO SKILL (SkillName,Note) VALUES('Dev', 'kha cham chi');
INSERT INTO SKILL (SkillName,Note) VALUES('Tester', 'cham chi');
INSERT INTO SKILL (SkillName,Note) VALUES('Tester', 'cham chi');
INSERT INTO SKILL (SkillName,Note) VALUES('BA', 'cham chi');

INSERT INTO EMP_SKILL (SkillNo, EmpNo, SkillLevel, RegDate) VALUES('3', '4', '3','5/1/2022');
INSERT INTO EMP_SKILL (SkillNo, EmpNo, SkillLevel, RegDate) VALUES('3', '5', '3','6/2/2022');
INSERT INTO EMP_SKILL (SkillNo, EmpNo, SkillLevel, RegDate) VALUES('3', '1', '3','7/4/2022');
INSERT INTO EMP_SKILL (SkillNo, EmpNo, SkillLevel, RegDate) VALUES('3', '2', '3','8/5/2022');
INSERT INTO EMP_SKILL (SkillNo, EmpNo, SkillLevel, RegDate) VALUES('3', '3', '3','9/6/2022');

CREATE VIEW EMPLOYEE_TRACKING
AS SELECT dbo.EMPLOYEE.Empno,dbo.EMPLOYEE.Empname,dbo.EMPLOYEE.Level FROM EMPLOYEE WHERE (Level >=3 AND Level <= 5);
